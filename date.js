function fixFormat(input) {
  if (input < 10) {
    return "0" + input;
  } else {
    return input;
  }
}

function currentDateTime() {
  const now = new Date();
  const year = now.getFullYear();
  const month = now.getMonth() + 1;
  const date = now.getDate();
  const hour = now.getHours();
  const minute = now.getMinutes();
  const seconds = now.getSeconds();
  return {
    date: `${year}-${fixFormat(month)}-${fixFormat(date)}`,
    time: `${fixFormat(hour)}:${fixFormat(minute)}:${fixFormat(seconds)}`
  };
}

module.exports.currentDateTime = currentDateTime;
