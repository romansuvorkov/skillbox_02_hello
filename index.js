const { currentDateTime } = require('./date');

const data = currentDateTime();
console.log(`Today is ${data.date}, the current time is ${data.time}`);
